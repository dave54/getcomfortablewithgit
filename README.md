# Get Comfortable with Git

Presentation Slides for:
- workshop for the [Kansas City Developer Conference](https://www.kcdc.info/) - Sep 15 2021
- talk at [Denver Dev Day](https://denverdevday.github.io/oct-2022/) - Oct 21, 2022

Resources:
- [The Presentation Slides](https://dave54.gitlab.io/getcomfortablewithgit/)
- [The Git Parable](https://tom.preston-werner.com/2009/05/19/the-git-parable.html)
- [Visualizaing Git](https://git-school.github.io/visualizing-git/)
- ["Yes silver bullet" - Mark Seemann](https://blog.ploeh.dk/2019/07/01/yes-silver-bullet/)
- ["The Git Book" - Scott Chacon and Ben Straub](https://git-scm.com/book/en/v2)
